class Hangman
  attr_reader :guesser, :referee
  attr_accessor :board

  def initialize(options = {})
    default = {
      guesser: HumanPlayer.new("Bob"),
      referee: ComputerPlayer.new
    }
    options = default.merge(options)

    @guesser = options[:guesser]
    @referee = options[:referee]
  end

  def setup
    @word_length = @referee.pick_secret_word
    @guesser.register_secret_length(@word_length)
    @board = ["_"] * @word_length
  end

  def take_turn
    guess = @guesser.guess
    arr = @referee.check_guess(guess)
    update_board(arr, guess)
    @guesser.handle_response(arr, guess)
  end

  def update_board(arr, guess)
    arr.each { |i| @board[i] = guess }
  end
end

class HumanPlayer
  attr_accessor :name

  def initialize(name)
    @name = name
  end

  def register_secret_length(length)
    p "The length of the word is #{length}."
  end

  def guess
    p "Guess a letter."
    gets.chomp
  end
  #
  def handle_response(arr, guess)
    if arr.length > 0
      p "Your guess of #{guess} occured #{arr.length} of times."
    else
      p "Sorry, the letter #{guess} doesn't exist in this word."
    end
  end
end

class ComputerPlayer
  attr_reader :dictionary

  def initialize(dictionary = File.readlines('lib/dictionary.txt').map(&:chomp))
    @dictionary = dictionary
  end

  def pick_secret_word
    @secret_word = @dictionary.shuffle[0]
    @secret_word.length
  end

  def check_guess(letter)
    @secret_word.each_char.with_index.inject([]) do |arr, (ch, i)|
      ch == letter ? arr << i : arr
    end
  end

  def register_secret_length(length)
    @dictionary = candidate_words.select { |word| word.length == length }
  end

  def guess(board)
    ("a".."z").to_a.sample
    letter_hash = Hash.new(0)
    candidate_words.each do |word|
      word.each_char do |ch|
        letter_hash[ch] += 1
      end
    end
    answer = letter_hash.reject { |k, _v| board.include?(k) }.values.max

    letter_hash.select { |k, v| return k if v == answer }

  end

  def candidate_words
    @dictionary
  end

  def handle_response(guess, arr)
    @dictionary = candidate_words.inject([]) do |array, word|
      if arr.all? { |i| word[i] == guess } && word.count(guess) == arr.length
        array << word
      else
        array
      end
    end
  end

end
